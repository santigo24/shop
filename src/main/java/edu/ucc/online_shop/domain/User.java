package edu.ucc.online_shop.domain;

import edu.ucc.online_shop.enums.UserRolEnum;

import javax.persistence.*;

@Entity
@Table(name = "user", schema = "shop")

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "user_rol", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRolEnum userRolEnum;

    @Column(name = "password")
    private String password;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "identification", nullable = false, unique = true)
    private String identification;

    @Column(name = "phone")
    private String phone;

    @Column(name = "lock_user")
    private Boolean lockUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRolEnum getUserRolEnum() {
        return userRolEnum;
    }

    public void setUserRolEnum(UserRolEnum userRolEnum) {
        this.userRolEnum = userRolEnum;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getLockUser() {
        return lockUser;
    }

    public void setLockUser(Boolean lockUser) {
        this.lockUser = lockUser;
    }
}
