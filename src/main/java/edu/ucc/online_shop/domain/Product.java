package edu.ucc.online_shop.domain;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@Table (name="product", schema="shop",  uniqueConstraints = {
        @UniqueConstraint(name = "unique_product_in_stock", columnNames = {"name", "price", "cantidad"})
})
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 150)
    private String name;

    @Column(name = "cantidad", nullable = false, length = 50)
    private Long cantidad;

    @Column(name = "price", nullable = false, length = 50)
    private Long price;

    @Column(name = "in_stock", nullable = false, length = 50)
    @Min(value = 0, message = "No quedan más productos en stock")// NO PUEDE SER NEGATIVO
    private Integer inStock;

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id", updatable = false, insertable = false)
    Category category;

    @Column(name = "category_id")
    private Long categoryId;


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
}
