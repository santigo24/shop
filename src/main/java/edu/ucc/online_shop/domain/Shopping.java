package edu.ucc.online_shop.domain;

import edu.ucc.online_shop.enums.ShoppingStateEnum;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.util.Date;


@Entity
@Table(name = "shopping", schema = "shop")
public class Shopping {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "id", nullable = false)
        private Long id;

        @ManyToOne
        @JoinColumn(name = "user_id", referencedColumnName = "id", updatable = false, insertable = false)
        User user;

        @Column(name = "user_id")
        private Long userId;

        @ManyToOne
        @JoinColumn(name = "product_id", referencedColumnName = "id", updatable = false, insertable = false)
        Product product;

        @Column(name = "product_id")
        private Long productId;

        @Column(name = "purchase_date")
        private Date purchaseDate;

        @Column(name = "date_entry")
        private Date dateEntry;

        @Column(name = "state", nullable = false)
        @Enumerated(EnumType.STRING)
        private ShoppingStateEnum shoppingStateEnum;

        @Column(name = "queue_wait")
        @Max(value = 5, message = "La cola de espera ya esta llena")
        private Long queueWait;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }

        public Long getProductId() {
            return productId;
        }

        public void setProductId(Long productId) {
            this.productId = productId;
        }

        public Date getPurchaseDate() {
            return purchaseDate;
        }

        public void setPurchaseDate(Date purchaseDate) {
            this.purchaseDate = purchaseDate;
        }

        public Date getDateEntry() {
            return dateEntry;
        }

        public void setDateEntry(Date dateEntry) {
            this.dateEntry = dateEntry;
        }

        public ShoppingStateEnum getShoppingStateEnum() { return shoppingStateEnum; }

        public void setShoppingStateEnum(ShoppingStateEnum shoppingStateEnum) { this.shoppingStateEnum = shoppingStateEnum; }

        public Long getQueueWait() { return queueWait; }

        public void setQueueWait(Long queueWait) {
            this.queueWait = queueWait;
        }
    }

