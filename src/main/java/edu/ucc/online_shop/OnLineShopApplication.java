package edu.ucc.online_shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnLineShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnLineShopApplication.class, args);
	}

}
