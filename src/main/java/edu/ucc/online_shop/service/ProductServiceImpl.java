package edu.ucc.online_shop.service;

import edu.ucc.online_shop.domain.Product;
import edu.ucc.online_shop.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;}

    @Override
    public Product findByPriceAndNameAndInStock(Long price, String name) {
        Optional<Product> optionalProduct = this.productRepository.findByPriceAndNameAndInStock(price, name);

        return optionalProduct.orElseGet(Product::new);
//        if (optionalProduct.isPresent())
//            return optionalProduct.get();
//        else
//            return new Product();
    }

    @Override
    public Product findByPriceAndName(Long price, String name) {
        Optional<Product> optionalProduct = this.productRepository.findByPriceAndName(price, name);

        return optionalProduct.orElseGet(Product::new);
    }

    @Override
    public List<Product> findAll() {
        return this.productRepository.findAll();
    }

    @Override
    public Product findById(Long id) {
//        Optional<Product> ProductOptional = this.ProductRepository.findById(id);
        return this.productRepository.findById(id).orElseGet(Product::new);
    }

    @Override
    public Product save(Product product) {
        return this.productRepository.save(product);
    }

    @Override
    public Product update(Product product, Long id) {
        if (this.productRepository.existsById(id))
            return this.productRepository.save(product);

        return null;
    }

    @Override
    public Boolean delete(Long id) {
        if (this.productRepository.existsById(id)) {
            this.productRepository.deleteById(id);
            return true;
        }

        return false;
    }
}