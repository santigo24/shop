package edu.ucc.online_shop.service;


import edu.ucc.online_shop.domain.Product;

import java.util.List;

public interface ProductService {
    Product findByPriceAndNameAndInStock (Long price, String name);
    Product findByPriceAndName(Long price, String name);
    List<Product> findAll();
    Product findById(Long id);
    Product save(Product product);
    Product update(Product product, Long id);
    Boolean delete(Long id);
}
