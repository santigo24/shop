package edu.ucc.online_shop.service;

import edu.ucc.online_shop.domain.Shopping;

import java.util.List;

public interface ShoppingService {
    Shopping create(Shopping shopping) throws Exception;
    List<Shopping> findAll();
    List<Shopping> findAllByProductId(Long productId);
}
