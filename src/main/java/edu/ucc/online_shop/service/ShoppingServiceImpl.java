package edu.ucc.online_shop.service;

import edu.ucc.online_shop.domain.Shopping;
import edu.ucc.online_shop.enums.ShoppingStateEnum;
import edu.ucc.online_shop.repository.ShoppingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ShoppingServiceImpl implements ShoppingService {
    private final ShoppingRepository shoppingRepository;

    public ShoppingServiceImpl(ShoppingRepository shoppingRepository) { this.shoppingRepository = shoppingRepository; }

    @Override
    public Shopping create(Shopping shopping) throws Exception {
        LocalDate localDate = LocalDate.now();

        shopping.setShoppingStateEnum(ShoppingStateEnum.ACTIVE);
        shopping.setPurchaseDate(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
        shopping.setDateEntry(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).plusDays(5).toInstant()));

        if (shopping.getQueueWait() != null && (shopping.getQueueWait() < 0 || shopping.getQueueWait() >= 5)) {
            // -1 , -2 6, 7, 8
            // ERROR
            throw new Exception("La cola esta llena");
        }

        return this.shoppingRepository.save(shopping);
    }

    @Override
    public List<Shopping> findAll() {
        return this.shoppingRepository.findAll();
    }

    @Override
    public List<Shopping> findAllByProductId(Long productId) { return this.shoppingRepository.findAllByProductId(productId);
    }
}
