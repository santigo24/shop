package edu.ucc.online_shop.controller;

import edu.ucc.online_shop.domain.Product;
import edu.ucc.online_shop.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService= productService;
    }

    @GetMapping("Products-search-stock")
    public ResponseEntity<Product> searchInStock(@RequestParam("price") Long price, @RequestParam("name") String name) {
        Product product = this.productService.findByPriceAndNameAndInStock(price, name);

        if (product.getId() != null)
            return new ResponseEntity<>(product, HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
    @GetMapping("Products-search")
    public ResponseEntity<Product> search(@RequestParam("price") Long price, @RequestParam("name") String name) {
        Product product = this.productService.findByPriceAndName(price, name);

        if (product.getId() != null)
            return new ResponseEntity<>(product, HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @GetMapping("Products")
    public ResponseEntity<List<Product>> findAll() {
        List<Product> ProductList = this.productService.findAll();

        if (!ProductList.isEmpty())
            return new ResponseEntity<>(ProductList, HttpStatus.OK);
        else
            return new ResponseEntity<>(ProductList, HttpStatus.NOT_FOUND);
    }

    @GetMapping("Products/{id}")
    public ResponseEntity<Product> findById(@PathVariable("id") Long id) {
        Product byId = this.productService.findById(id);

        if (byId.getId() != null)
            return new ResponseEntity<>(byId, HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @PostMapping("Product")
    public ResponseEntity<Product> create(@Valid @RequestBody Product product) {
        Product save = this.productService.save(product);

        if (save.getId() != null)
            return new ResponseEntity<>(save, HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @PutMapping("Product/{id}")
    public ResponseEntity<Product> update(@Valid @RequestBody Product product, @PathVariable("id") Long id) {
        Product update = this.productService.update(product, id);

        if (update != null)
            return new ResponseEntity<>(update, HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("Product/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        boolean delete = this.productService.delete(id);

        if (delete)
            return new ResponseEntity<>("eliminado", HttpStatus.OK);
        else
            return new ResponseEntity<>("Fallo Eliminación", HttpStatus.NOT_FOUND);
    }
}