package edu.ucc.online_shop.controller;

import edu.ucc.online_shop.domain.Shopping;
import edu.ucc.online_shop.service.ShoppingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ShoppingController {

    private final ShoppingService shoppingService;

    public ShoppingController(ShoppingService shoppingService) {
        this.shoppingService = shoppingService;
    }

    @GetMapping("shoppings")
    public ResponseEntity<List<Shopping>> findAll() {
        return new ResponseEntity<>(this.shoppingService.findAll(), HttpStatus.OK);
    }

    // TODO probar en postman
    @GetMapping("shoppings-search")
    public ResponseEntity<List<Shopping>> ShoppingSearch(@RequestParam("productId") Long productId) {
        return new ResponseEntity<>(this.shoppingService.findAllByProductId(productId), HttpStatus.OK);
    }

    // TODO probar en postman
    @PostMapping("shopping")
    public ResponseEntity<Shopping> create(@Valid @RequestBody Shopping shopping) {
        try {
            Shopping shoppingCreate = this.shoppingService.create(shopping);

            if (shoppingCreate.getId() != null)
                return new ResponseEntity<>(shoppingCreate, HttpStatus.OK);
            else
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }
}

