package edu.ucc.online_shop.enums;

public enum UserRolEnum {
    ADMIN, CLIENT, SALESMAN
}
