package edu.ucc.online_shop.enums;

public enum ShoppingStateEnum {
    ACTIVE, EXPIRY, RETURNED, POSTPONE, PENDING
}
