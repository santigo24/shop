package edu.ucc.online_shop.repository;

import edu.ucc.online_shop.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    // metodos funcionales -> que tengan utilidad
    // Yo como BB quiero listar todos los productos que tengo en la tienda
    // 1 Query creation from method names
    List<Product> findAll(); // optimzación realizada springboot

    // JPQL
    // 2 Query by jpql
    @Query(value = "select b from Product b")
    List<Product> getAllProductsByJpql();

    // 3 Query native
    @Query(value = "select b.* from product b", nativeQuery = true)
    List<Product> getAllProductsNativeQuery();

    List<Product> findAllByPrice(Long price);

    @Query(value = "select b from Product b where b.price = :price and b.inStock > 0 and b.name = :name")
    Optional<Product> findByPriceAndNameAndInStock(@Param("price") Long price, @Param("name") String name);

    @Query(value = "select b from Product b where b.price = :price and b.name = :name")
    Optional<Product> findByPriceAndName(@Param("price") Long price, @Param("name") String name);

    @Query(value = "select b.* from product b where b.price = :x", nativeQuery = true)
    List<Product> findAllByPriceNativeQuery(@Param("x") Long price);

    List<Product> findAllByPriceOrderById(Long price); // order by id asc

    List<Product> findAllByPriceOrderByIdDesc(Long price); // order by id desc

    List<Product> findAllByPriceAndCategoryIdOrderById(Long price, Long categoryId); // precious and category id

    // CRUD -> Create Update y el Delete

}
