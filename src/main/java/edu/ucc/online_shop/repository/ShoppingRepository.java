package edu.ucc.online_shop.repository;

import edu.ucc.online_shop.domain.Shopping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ShoppingRepository extends JpaRepository<Shopping, Long> {

    @Query(value = "select l from Shopping l where l.productId = :productId")
    List<Shopping> findAllByProductId(@Param("productId") Long productId);

}
